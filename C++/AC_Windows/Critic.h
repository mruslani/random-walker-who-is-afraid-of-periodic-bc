#pragma once

#include <torch/torch.h>
using namespace std;

class CriticImpl : public torch::nn::Module {
    public:
        CriticImpl(int input_dims, int hidden_size);

        torch::Tensor forward(torch::Tensor state);

    private:
        torch::nn::Linear v1;
        torch::nn::Linear v;
};

TORCH_MODULE(Critic);