#pragma once

#include <torch/torch.h>
using namespace std;

class ActorImpl : public torch::nn::Module {
    public:
        ActorImpl(int input_dims, int hidden_size, int n_actions);
        
        torch::Tensor forward(torch::Tensor state);

    private:
        torch::nn::Linear pi1;
        torch::nn::Linear pi;
};

TORCH_MODULE(Actor);