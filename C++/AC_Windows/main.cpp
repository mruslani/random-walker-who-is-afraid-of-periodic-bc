// C++ version of Actor Critic algorithm implementation, using random walkers that avoid 
// periodic boundary as a learning test example

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <random>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <math.h>
#include <torch/torch.h>
#include "Actor.h"
#include "Critic.h"

using namespace std;
namespace F = torch::nn::functional;

int main(){

    // Device
    auto cuda_available = torch::cuda::is_available();
    torch::Device device(cuda_available ? torch::kCUDA : torch::kCPU);
    cout << (cuda_available ? "CUDA available. Training on GPU." : "Training on CPU.") << '\n';

    // simulation parameters
    const int L =  20;
    const int T_MAX = 1000;
    const int T_UPDATE = 10; // 20 // defines the length of the episode, after which all network parameters are updated
    const int N_RUNS = 10; // 100;
    const double INV_N_RUNS = 1.0/(1.0*N_RUNS);
    double lr = 0.001; // learning rate
    double gamma = 1.0;
    const int n_actions = 2;
    const int observation_radius = 2;
    int input_dims = observation_radius*2 + 1;
    const int hidden_size = 10;
    int n_of_snapshots = int(T_MAX/T_UPDATE);
    // RNG
    random_device rd{};
    mt19937 RNG{ rd() };
    uniform_real_distribution<double> dice{ 0, 1 }; // a dice from 0 to 1
    uniform_int_distribution<int> init_dist{0, L - 1}; // an integer dice from 0 to L-1
    uniform_int_distribution<int> coin(0,1); 
    // output data
    vector<double> total_score(n_of_snapshots, 0); 
    vector<double> total_loss(n_of_snapshots, 0); 
    vector<vector<double>> pdf(L, vector<double> (n_of_snapshots, 0)); // RW probability distribution 

    // collect statistics over different runs
    int run = 0;
    while (run < N_RUNS) {
        cout << "Run: " << run << endl;
        int flip_coin = coin(RNG);
        int score = 0; double record_loss = 0;

        Actor actor(input_dims, hidden_size, n_actions);
        Critic critic(input_dims, hidden_size);
        actor->to(device);
        critic->to(device);
        torch::optim::Adam optimizer_actor(actor->parameters(), torch::optim::AdamOptions(lr));
        torch::optim::Adam optimizer_critic(critic->parameters(), torch::optim::AdamOptions(lr));
        // record observations
        vector<torch::Tensor> observations;
        vector<int> actions;
        vector<int> rewards;
        // initial condition: place a walker somewhere randomly on a lattice
        int walker_position = init_dist(RNG); 

        // main simulation loop
        for(int timestep = 1; timestep <= T_MAX; timestep++){
            // get the observation
            torch::Tensor observation = torch::zeros({1,input_dims});
            for(int i = 0; i < input_dims; i++){
                int coordinate = -1;
                // implement periodic boundary conditions for observed state
                if(walker_position - observation_radius + i < 0){
                    coordinate = walker_position - observation_radius + i + L;
                }
                else if(walker_position - observation_radius + i > L-1){
                    coordinate = walker_position - observation_radius + i - L;
                }
                else{
                    coordinate = walker_position - observation_radius + i;
                }
                if(coordinate == -1){
                    cout << "Error! Coordinate is not assigned properly!"<<endl;
                    return 1;
                }
                observation[0][i] = coordinate;
            }
            // select the action
            auto Pi = actor->forward(observation);
            auto probs = F::softmax(Pi, F::SoftmaxFuncOptions(1));
            double sum = probs[0][0].item<double>(); 
            int action = 0;
            double dice_throw = dice(RNG);
            while (sum < dice_throw && action < n_actions) {
                action++;
                sum += probs[0][action].item<double>();
            }
            if (action >= n_actions) {
                cout << "Error! Action number is greater or equal than n_actions!" << endl;
                // potential problem: always use the same ordering 1..K for mapping probabilities on [0,1] line
                cout << "returning action " << action << endl << endl;
                return 1;
            }
            // update the position and record the reward
            int delta_x;
            if(flip_coin == 0) { // to eliminate bias
                delta_x = action == 0 ? +1 : -1; // for 0 jump right, for 1 jump left
            }
            else{
                delta_x = action == 0 ? -1 : 1; // for 1 jump right, for 0 jump left
            }
            if(walker_position + delta_x > L-1){
                walker_position = walker_position + delta_x - L;
                //reward = -100; // punish the walker if it crosses the periodic boundary
            }
            else if(walker_position + delta_x < 0){
                walker_position = walker_position + delta_x + L;
                //reward = -100; // punish the walker if it crosses the periodic boundary
            }
            else{
                walker_position += delta_x;
                //reward = 1; // reward the walker for staying away from the boundary
            }
            int reward = delta_x == +1 ? 1 : -100; // reward for moving to the right, and punish for moving to the left
            observations.push_back(observation);
            actions.push_back(action);
            rewards.push_back(reward);
            
            // update the network 
            if(timestep % T_UPDATE == 0 && timestep > 0){

                //cout << "array of states" << endl << observations << endl;
                //cout << "array of probs" << endl;
                torch::Tensor log_probs = torch::zeros({ T_UPDATE,1 });
                torch::Tensor V = torch::zeros({ T_UPDATE,1 });
                for (auto i = 0; i < T_UPDATE; i++) { // _states.size() has the same length as the episode length
                    auto pi_i = actor->forward(observations[i]);
                    auto v_i = critic->forward(observations[i]);
                    auto probs = F::softmax(pi_i, F::SoftmaxFuncOptions(1)); // converts list of logits to list of probabilities
                    int action = actions[i];
                    auto log_prob = log(probs[0][action]);
                    log_probs[i] = log_prob;
                    V[i] = v_i[0];
                    //cout << probs << "\t";
                }
                //cout << endl;
                //cout << "array of actions" << endl << actions << endl;
                //cout << "array of critic estimates" << endl << V << endl;
                //cout << "array of rewards " << endl << rewards << endl << endl;

                //double R = V.back().item<double>(); // take last V element for Q computation? 
                double R = 0;
                torch::Tensor returns = torch::zeros({ T_UPDATE,1 });
                for (auto i = 0; i < T_UPDATE - 1; i++) {
                    R += pow(gamma, i) * rewards[i];
                    returns[i] = R;
                }
                returns[T_UPDATE - 1] = R + pow(gamma, T_UPDATE - 1) * V[T_UPDATE - 1];
                //cout << "returns" << endl << returns << endl;

                auto actor_loss = -log_probs[0] * (returns[0] - V[0]);
                auto critic_loss = pow(returns[0] - V[0], 2);
                //cout << "actor_loss " << actor_loss << " critic_loss " << critic_loss << endl;
                for (auto i = 1; i < T_UPDATE; i++) {
                    actor_loss += -log_probs[i] * (returns[i] - V[i]);
                    critic_loss += pow(returns[i] - V[i], 2);
                }
                //cout << "total_loss " << (actor_loss + critic_loss) / (1.0*_states.size()) << endl;           
                optimizer_actor.zero_grad(); // Reset gradients
                optimizer_critic.zero_grad(); // Reset gradients
                auto loss = (actor_loss + critic_loss) / (1.0 * T_UPDATE);
                loss.backward(); // backpropagate loss
                optimizer_actor.step(); // update network parameters
                optimizer_critic.step(); // update network parameters
                observations.clear();
                actions.clear();
                rewards.clear();
                // record output
                total_score[timestep/T_UPDATE - 1] += (1.0*score)*INV_N_RUNS;
                total_loss[timestep/T_UPDATE - 1] += loss.item<double>()*INV_N_RUNS;
                pdf[walker_position][timestep/T_UPDATE - 1] += INV_N_RUNS; // record positiposition; danger of exceeding int_max for lots or runs
            }
            score += reward;
        }
        //cout << "updated model parameters" << endl << model->parameters() << endl;
        run++;
    }
    cout << "Simulation finished" << endl;
    cout << endl;

    // show time vs reward
    cout << "How reward and loss change with time: " << endl;
    for(int timestep = 0; timestep < T_MAX/T_UPDATE; timestep++){
        cout << (timestep + 1)*T_UPDATE << "\t" << total_score[timestep] << 
            "\t" << total_loss[timestep] << endl;
    }
    cout << endl;

    // output rewards
    string reward_filename = "output/Reward_L" + to_string(L) + "_TMAX" + to_string(T_MAX) + 
        "_TUP" + to_string(T_UPDATE) + "_RUNS" + to_string(N_RUNS) + ".txt";
    ofstream output_reward(reward_filename);
    copy(total_score.begin(), total_score.end(), ostream_iterator<double>(output_reward, "\n"));

    // output loss
    string loss_filename = "output/Loss_L" + to_string(L) + "_TMAX" + to_string(T_MAX) + 
        "_TUP" + to_string(T_UPDATE) + "_RUNS" + to_string(N_RUNS) + ".txt";
    ofstream output_loss(loss_filename);
    copy(total_loss.begin(), total_loss.end(), ostream_iterator<double>(output_loss, "\n"));

     // normalize pdfs
    for(int snapshot = 0; snapshot < n_of_snapshots; snapshot++){
        double sum = 0;
        for(int x = 0; x < L; x++){
            sum += pdf[x][snapshot];
        }
        for(int x = 0; x < L; x++){
            pdf[x][snapshot] /= sum;
        }
    }   
    // output pdfs
    string pdf_filename = "output/PDF_L" + to_string(L) + "_TMAX" + to_string(T_MAX) + 
        "_TUP" + to_string(T_UPDATE) + "_RUNS" + to_string(N_RUNS) + ".txt";
    ofstream output_pdf(pdf_filename);
    ostream_iterator<double> output_iterator(output_pdf, "\t");
    for (int i = 0; i < pdf.size(); i++){
        copy(pdf.at(i).begin(), pdf.at(i).end(), output_iterator);
        output_pdf << "\n";
    }

    cin.get();

    return 0;
}