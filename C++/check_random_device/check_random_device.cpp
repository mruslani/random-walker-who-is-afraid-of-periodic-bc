#include <iostream>
#include <random>
#include <chrono>

using namespace std;

int main(){
	
	auto begin = std::chrono::high_resolution_clock::now();
	
	random_device rd{};
	mt19937 RNG{ rd() };

	int total_n_of_calls = 1e6;
	vector<int> seeds;
	
	for(auto call = 0; call < total_n_of_calls; call++){
		int call_rd = rd();
		seeds.push_back(call_rd);
	}
	
	int count_repeats = 0;
	sort(seeds.begin(), seeds.end());
	for(int i = 0; i < seeds.size() - 1; i++) {
		if (seeds[i] == seeds[i + 1]) {
			count_repeats++;
		}
	}
	
	printf("Number of times random_device have been called: %.i\n", total_n_of_calls);
	printf("Number of repeats: %i\n", count_repeats);

	auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    printf("Duration: %.3f seconds.\n", elapsed.count() * 1e-9);

    return 0;
}