#include "Actor.h"
#include "Critic.h"
#include <torch/torch.h>

using namespace std;
namespace F = torch::nn::functional;

ActorImpl::ActorImpl(int input_dims, int hidden_size, int n_actions)
    : pi1(input_dims, hidden_size), pi(hidden_size, n_actions){
    register_module("pi1", pi1);
    register_module("pi", pi);
}

CriticImpl::CriticImpl(int input_dims, int hidden_size)
    : v1(input_dims, hidden_size), v(hidden_size, 1) {
    register_module("v1", v1);
    register_module("v", v);
}

torch::Tensor ActorImpl::forward(torch::Tensor state) {
    auto hidden_pi = F::relu(pi1->forward(state));
    return pi->forward(hidden_pi);
}

torch::Tensor CriticImpl::forward(torch::Tensor state) {
    auto hidden_v = F::relu(v1->forward(state));
    return v->forward(hidden_v);
}

