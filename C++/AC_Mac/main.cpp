// C++ version of Actor Critic algorithm implementation, using random walkers that avoid 
// periodic boundary as a learning test example
#include <iostream>
#include <chrono>
#include <iomanip>
#include <stdio.h>
#include <random>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <math.h>
#include <torch/torch.h>
#include "Actor.h"
#include "Critic.h"
using namespace std;
namespace F = torch::nn::functional;

// Random Number Generator
random_device rd{};
mt19937 RNG{ rd() };
uniform_real_distribution<double> dice{ 0, 1 }; // a dice from 0 to 1
uniform_int_distribution<int> coin(0,1); 

void get_observation_state(const int L, const int observation_radius, int input_dims, int walker_position, torch::Tensor observation){
    for(int i = 0; i < input_dims; i++){
        int coordinate = -1;
        // implement periodic boundary conditions for observed state
        if(walker_position - observation_radius + i < 0){
            coordinate = walker_position - observation_radius + i + L;
        }
        else if(walker_position - observation_radius + i > L-1){
            coordinate = walker_position - observation_radius + i - L;
        }
        else{
            coordinate = walker_position - observation_radius + i;
        }
        if(coordinate == -1){
            cout << "Error! Coordinate is not assigned properly!"<<endl;
        }
        observation[0][i] = coordinate; 
    }
}

int get_reward(const int L, int flip_coin, int action, int& walker_position){
    int delta_x; int reward;
    if(flip_coin == 0) { // to eliminate bias
        delta_x = action == 0 ? +1 : -1; // for 0 jump right, for 1 jump left
    }
    else{
        delta_x = action == 0 ? -1 : 1; // for 1 jump right, for 0 jump left
    }
    if(walker_position + delta_x > L-1){
        walker_position = walker_position + delta_x - L;
        reward = -100; // punish the walker if it crosses the periodic boundary
    }
    else if(walker_position + delta_x < 0){
        walker_position = walker_position + delta_x + L;
        reward = -100; // punish the walker if it crosses the periodic boundary
    }
    else{
        walker_position += delta_x;
        reward = 1; // reward the walker for staying away from the boundary
    }
    //int reward = delta_x == +1 ? 1 : -100; // reward for moving to the right, and punish for moving to the left
    return reward;
}

int main(int argc, char* argv[]){
	auto begin = std::chrono::high_resolution_clock::now();
    // Device
    auto cuda_available = torch::cuda::is_available();
    torch::Device device(cuda_available ? torch::kCUDA : torch::kCPU);
    cout << (cuda_available ? "CUDA available. Training on GPU." : "Training on CPU.") << '\n';
    // simulation parameters
    const int L = atoi(argv[1]); // 20;
    const int T_MAX = atoi(argv[2]); // 1000;
    const int T_UPDATE = atoi(argv[3]); // 20 // defines the length of the episode, after which all network parameters are updated
    const int N_RUNS = atoi(argv[4]); // 100;
    const double INV_N_RUNS = 1.0/(1.0*N_RUNS);
    double lr = atof(argv[5]); // learning rate
    double gamma = 1.0;
    const int n_actions = 2; // jump left or right
    const int observation_radius = 2;
    int input_dims = observation_radius*2 + 1; // size of the observation state
    const int hidden_size = 10; // size of the hidden layer
    int n_of_snapshots = int(T_MAX/T_UPDATE);
    // memory allocation for output data
    vector<double> total_score(n_of_snapshots, 0); 
    vector<double> total_loss(n_of_snapshots, 0); 
    vector<vector<double>> pdf(L, vector<double> (n_of_snapshots, 0)); // RW probability distribution 
    vector<vector<double>> score_per_run(n_of_snapshots, vector<double> (N_RUNS, 0)); // RW probability distribution 
    vector<vector<double>> loss_per_run(n_of_snapshots, vector<double> (N_RUNS, 0)); // RW probability distribution 

    for(auto run = 0; run < N_RUNS; run++) { // collect statistics over different runs
        cout << "Run: " << run << endl;
        uniform_int_distribution<int> init_dist{0, L - 1}; // an integer dice from 0 to L-1
        int walker_position = init_dist(RNG); // initial condition: place a walker somewhere randomly on a lattice
        int flip_coin = coin(RNG); // decide whether Actor's output [0,1] elements will correspond to [left,right] jupms or vice versa
        double score = 0; double record_loss = 0; // keep track on reward & loss changes with time
        // create instances of Actor and Critic networks
        Actor actor(input_dims, hidden_size, n_actions);
        Critic critic(input_dims, hidden_size);
        actor->to(device);
        critic->to(device);
        torch::optim::Adam optimizer_actor(actor->parameters(), torch::optim::AdamOptions(lr));
        torch::optim::Adam optimizer_critic(critic->parameters(), torch::optim::AdamOptions(lr));
        // record observations
        vector<torch::Tensor> observations;
        vector<int> actions;
        vector<int> rewards;

        for(int timestep = 1; timestep <= T_MAX; timestep++){ // main simulation loop
            torch::Tensor observation = torch::zeros({1,input_dims});
            get_observation_state(L, observation_radius, input_dims, walker_position, observation); // obtain the observation state
            auto Pi = actor->forward(observation); // pass observation to Actor, which performs forward prop and produces a set of logits as output
            auto probs = F::softmax(Pi, F::SoftmaxFuncOptions(1)); // is it doing the right thing? CHECK!!! should convert logits to probs
            double dice_throw = dice(RNG);
            int action = dice_throw < probs[0][0].item<double>() ? 0 : 1; // select action
            int reward = get_reward(L, flip_coin, action, walker_position); // update the position and record the reward
            // record the observation state, action, and corresponding reward
            observations.push_back(observation);
            actions.push_back(action);
            rewards.push_back(reward);
            score += reward;
            
            // update the network parameters, i.e., do backprop
            if(timestep % T_UPDATE == 0 && timestep > 0){
                torch::Tensor log_probs = torch::zeros({ T_UPDATE,1 }); // here T_UPDATE is the length of the episode
                torch::Tensor V = torch::zeros({ T_UPDATE,1 });
                torch::Tensor returns = torch::zeros({ T_UPDATE,1 });
                double R = 0;
                for (auto i = 0; i < T_UPDATE; i++) { // _states.size() has the same length as the episode length
                    auto pi_i = actor->forward(observations[i]);
                    auto v_i = critic->forward(observations[i]);
                    auto probs = F::softmax(pi_i, F::SoftmaxFuncOptions(1)); // converts list of logits to list of probabilities
                    int action = actions[i];
                    auto log_prob = log(probs[0][action]);
                    log_probs[i] = log_prob;
                    V[i] = v_i[0];
                    R += pow(gamma, i) * rewards[i];
                    returns[i] = R;
                }
                returns[T_UPDATE - 1] = R - pow(gamma, T_UPDATE - 1) * rewards[T_UPDATE - 1] + pow(gamma, T_UPDATE - 1) * V[T_UPDATE - 1]; // gets a Q value             
                // just to properly initialize the losses variable type and connect it with AC network output do this:
                auto actor_loss = log_probs[0] * (returns[0] - V[0]); 
                auto critic_loss = pow(returns[0] - V[0], 2);
                for (auto i = 1; i < T_UPDATE; i++) { // then start with i=1
                    actor_loss += -log_probs[i] * (returns[i] - V[i]);
                    critic_loss += pow(returns[i] - V[i], 2);
                }
                optimizer_actor.zero_grad(); // Reset gradients
                optimizer_critic.zero_grad(); // Reset gradients
                auto loss = (actor_loss + critic_loss) / (1.0 * T_UPDATE); // take average
                loss.backward(); // backpropagate loss
                optimizer_actor.step(); // update actor network parameters
                optimizer_critic.step(); // update critic network parameters
                // record output
                total_score[timestep/T_UPDATE - 1] += score * INV_N_RUNS;
                score_per_run[timestep/T_UPDATE - 1][run] = score;
                loss_per_run[timestep/T_UPDATE - 1][run] = loss.item<double>();
                total_loss[timestep/T_UPDATE - 1] += loss.item<double>()*INV_N_RUNS;
                pdf[walker_position][timestep/T_UPDATE - 1] += INV_N_RUNS; // record positiposition; danger of exceeding int_max for lots or runs
                // free up the memory
                observations.clear();
                actions.clear();
                rewards.clear();
            }
        }
    }
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    printf("C++ simulation duration: %.3f seconds.\n", elapsed.count() * 1e-9);

    // show time vs total reward and total loss
    /*cout << "How reward and loss change with time: " << endl;
    for(int timestep = 0; timestep < T_MAX/T_UPDATE; timestep++){
        cout << (timestep + 1)*T_UPDATE << "\t" << total_score[timestep] << 
            "\t" << total_loss[timestep] << endl;
    }
    cout << endl;*/

    // output total averaged rewards and loss
    string reward_loss_filename = "output/Reward_Loss_L" + to_string(L) + "_TMAX" + to_string(T_MAX) + 
        "_TUP" + to_string(T_UPDATE) + "_RUNS" + to_string(N_RUNS) + ".txt";
    ofstream output_reward(reward_loss_filename);
    for(auto t = 0; t < total_score.size(); t++){
        output_reward << t*T_UPDATE << "\t" << total_score[t] << "\t" << total_loss[t] << endl;
    }

    // output rewards per run
    string reward_per_run_filename = "output/Reward_per_Run_L" + to_string(L) + "_TMAX" + to_string(T_MAX) + 
        "_TUP" + to_string(T_UPDATE) + "_RUNS" + to_string(N_RUNS) + ".txt";
    ofstream output_score_per_run(reward_per_run_filename);
    ostream_iterator<double> output_iterator(output_score_per_run, "\t");
    for (int i = 0; i < score_per_run.size(); i++){
        copy(score_per_run.at(i).begin(), score_per_run.at(i).end(), output_iterator);
        output_score_per_run << "\n";
    }

    // output loss per run
    string loss_per_run_filename = "output/Loss_per_Run_L" + to_string(L) + "_TMAX" + to_string(T_MAX) + 
        "_TUP" + to_string(T_UPDATE) + "_RUNS" + to_string(N_RUNS) + ".txt";
    ofstream output_loss_per_run(loss_per_run_filename);
    ostream_iterator<double> output_iterator2(output_loss_per_run, "\t");
    for (int i = 0; i < loss_per_run.size(); i++){
        copy(loss_per_run.at(i).begin(), loss_per_run.at(i).end(), output_iterator2);
        output_loss_per_run << "\n";
    }

    // output pdfs
    string pdf_filename = "output/PDF_L" + to_string(L) + "_TMAX" + to_string(T_MAX) + 
        "_TUP" + to_string(T_UPDATE) + "_RUNS" + to_string(N_RUNS) + ".txt";
    ofstream output_pdf(pdf_filename);
    ostream_iterator<double> output_iterator3(output_pdf, "\t");
    for (int i = 0; i < pdf.size(); i++){
        copy(pdf.at(i).begin(), pdf.at(i).end(), output_iterator3);
        output_pdf << "\n";
    }

    return 0;
}