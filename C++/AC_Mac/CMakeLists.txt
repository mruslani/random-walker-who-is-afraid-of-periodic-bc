cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(AC_Mac)

find_package(Torch REQUIRED)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${TORCH_CXX_FLAGS}")

add_executable(AC_Mac main.cpp ActorCritic.cpp)
target_link_libraries(AC_Mac "${TORCH_LIBRARIES}")
set_property(TARGET AC_Mac PROPERTY CXX_STANDARD 14)
