#pragma once

#include <torch/torch.h>
using namespace std;

class ActorCriticImpl : public torch::nn::Module {
    public:
        ActorCriticImpl(int input_dims, int hidden_size, int n_actions);
        
        torch::Tensor forwardActor(torch::Tensor state);
        torch::Tensor forwardCritic(torch::Tensor state);
        int choose_action(torch::Tensor observation);
        torch::Tensor calculate_loss(double gamma);
        void remember(torch::Tensor state, int action, int reward);
        void clear_memory();


    private:
        int _n_actions;
        int _input_dims;
        int _hidden_size;
        torch::nn::Linear pi1;
        torch::nn::Linear pi;
        torch::nn::Linear v1;
        torch::nn::Linear v;

        vector<torch::Tensor> _states;
        vector<double> _rewards;
        vector<int> _actions;
};

TORCH_MODULE(ActorCritic);