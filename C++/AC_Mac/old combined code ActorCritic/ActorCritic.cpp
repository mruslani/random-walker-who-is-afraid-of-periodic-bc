#include "ActorCritic.h"
#include <torch/torch.h>
#include <algorithm>
#include <math.h>
#include <random>

using namespace std;
namespace F = torch::nn::functional;

random_device rd2{};
mt19937 RNG2{ rd2() };
uniform_real_distribution<double> dice{0.0, 1.0}; // a dice from 0 to 1

ActorCriticImpl::ActorCriticImpl(int input_dims, int hidden_size, int n_actions)
    : pi1(input_dims, hidden_size), pi(hidden_size, n_actions),
        v1(input_dims, hidden_size),  v(hidden_size, 1){
    register_module("pi1", pi1);
    register_module("pi", pi);
    register_module("v1", v1);
    register_module("v", v);

    _input_dims = input_dims;
    _hidden_size = hidden_size;
    _n_actions = n_actions;
}


torch::Tensor ActorCriticImpl::forwardActor(torch::Tensor state) {
    auto hidden_layer = F::relu(pi1->forward(state));
    return pi->forward(hidden_layer);
}

torch::Tensor ActorCriticImpl::forwardCritic(torch::Tensor state) {
    auto hidden_layer = F::relu(v1->forward(state));
    return v->forward(hidden_layer);
}

int ActorCriticImpl::choose_action(torch::Tensor observation){
    auto Pi = forwardActor(observation);
    torch::Tensor probs = F::softmax(Pi, F::SoftmaxFuncOptions(1));  
    double dice_throw = dice(RNG2);
    double sum = probs[0][0].item<double>(); int k = 0;
    while (sum < dice_throw && k < _n_actions){
        k++;
        sum += probs[0][k].item<double>();
    }
    if(k >= _n_actions){
        cout << "Error! k is greater than n_actions!" << endl;
            cout << "dice = " << dice_throw << ", p[0] = " << probs[0][0].item<double>()
                    <<", p[1] = " << probs[0][1].item<double>() << endl;
        // potential problem: always use the same ordering 1..K for mapping probabilities on [0,1] line
        cout << "returning action " << k << endl << endl;
        return 1;
    }

    return k; // the action
}

torch::Tensor ActorCriticImpl::calculate_loss(double gamma){
    int states_size = _states.size();
    //cout << "array of states" << endl << _states << endl;
    //cout << "array of probs and actions" << endl;
    torch::Tensor log_probs = torch::zeros({states_size,1});
    torch::Tensor V = torch::zeros({states_size,1});
    for(auto i = 0; i < _actions.size(); i++){ // _states.size() has the same length as the episode length
        auto pi_i = forwardActor(_states[i]);
        auto v_i = forwardCritic(_states[i]);
        auto probs = F::softmax(pi_i, F::SoftmaxFuncOptions(1)); // converts list of logits to list of probabilities
        int action = _actions[i];
        auto log_prob = log(probs[0][action]);
        log_probs[i] = log_prob;
        V[i] = v_i[0];
        //cout << probs << "\t action " << action << endl;
    }
    //cout << "array of critic estimates" << endl << V << endl;
    //cout << "array of rewards " << endl << _rewards << endl << endl;

    //double R = V.back().item<double>(); // take last V element for Q computation? 
    double R = 0;
    torch::Tensor returns = torch::zeros({states_size,1});
    for(auto i = 0; i < _rewards.size() - 1; i++){
        R += pow(gamma,i)*_rewards[i];
        returns[i] = R;
    }
    returns[states_size - 1] = R + pow(gamma,states_size-1)*V[states_size-1];
    //cout << "returns" << endl << returns << endl;

    //auto loss = 0; 
    auto actor_loss = -log_probs[0]*(returns[0] - V[0]); 
    auto critic_loss = pow(returns[0] - V[0], 2);
    //cout << "actor_loss " << actor_loss << " critic_loss " << critic_loss << endl;
    for(auto i = 1; i < _states.size(); i++){
        actor_loss  += -log_probs[i]*(returns[i] - V[i]);
        critic_loss += pow(returns[i] - V[i], 2);
    }
    //cout << "total_loss " << (actor_loss + critic_loss) / (1.0*_states.size()) << endl;

    return (actor_loss + critic_loss) / (1.0*_states.size());
}
        
void ActorCriticImpl::remember(torch::Tensor state, int action, int reward){
    _states.push_back(state);
    _actions.push_back(action);
    _rewards.push_back(reward);
}

void ActorCriticImpl::clear_memory(){
    _states.clear();
    _actions.clear();
    _rewards.clear();
}

