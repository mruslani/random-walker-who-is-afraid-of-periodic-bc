# Random walker who is afraid of periodic boundary conditions

MIGRATED TO GITHUB:
https://github.com/RusFortunat/Random-walker-who-is-afraid-of-periodic-bc

This will be a project, where I will test some implementations of RL Actor-Critic algorithm, using a random walker, who hops on a 1d chain, and who is genuinely afraid of crossing the periodic boundary. 

I will first try implementing this folly in python using PyTorch ML library, and then i will try implementing it in C++ with PyTorch too, to see if there will be a reasonable speedup in the execution time.

Wish me luck boys. 
